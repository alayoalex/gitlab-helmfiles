{{- range $_, $instance := .Values.instances }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "pubsubbeat.fullname" $ }}-{{ $instance.topic }}
  labels:
    {{- include "pubsubbeat.labels" $ | nindent 4 }}
    topic: {{ $instance.topic }}
  annotations:
    {{- toYaml $.Values.deployment.annotations | nindent 4 }}
spec:
  {{- if not $.Values.autoscaling.enabled }}
  replicas: {{ $instance.replicas | default 1 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "pubsubbeat.selectorLabels" $ | nindent 6 }}
      topic: {{ $instance.topic }}
  template:
    metadata:
      labels:
        {{- include "pubsubbeat.labels" $ | nindent 8 }}
        topic: {{ $instance.topic }}
      annotations:
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") $ | sha256sum }}
        checksum/config-mtail: {{ include (print $.Template.BasePath "/configmap_mtail.yaml") $ | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") $ | sha256sum }}
        cluster-autoscaler.kubernetes.io/safe-to-evict: "true"
        {{- if $.Values.deployment.annotations }}
        {{- toYaml $.Values.deployment.annotations | nindent 8 }}
        {{- end }}
    spec:
      {{- if or $.Values.serviceAccount.create $.Values.serviceAccount.name }}
      serviceAccountName: {{ template "pubsubbeat.serviceAccountName" $ }}
      {{- end }}
      containers:
        - name: {{ include "pubsubbeat.fullname" $ }}
          image: "{{ $.Values.deployment.image.repository }}:{{ $.Values.deployment.image.tag }}"
          imagePullPolicy: {{ $.Values.deployment.image.pullPolicy }}
          command: ["/bin/sh"]
          args: ["-c", "/bin/pubsubbeat -c /etc/configmap/pubsubbeat.yml -e 2>&1 | /usr/bin/rotatelogs -e /volumes/emptydir/pubsubbeat.log 50M"]
          env:
          {{- range $key, $value := $.Values.deployment.env }}
            - name: "{{ $key }}"
              value: "{{ $value }}"
          {{- end }}
          envFrom:
            - secretRef:
                name: pubsubbeat
          ports:
            - name: "stats"
              containerPort: 5066
          readinessProbe:
            httpGet:
              port: "stats"
              path: "/stats"
          resources:
            {{- toYaml $.Values.deployment.resources | nindent 12 }}
          volumeMounts:
            - name: config-files
              mountPath: /etc/configmap
            - name: emptydir
              mountPath: /volumes/emptydir/
        - name: beat-exporter
          image: "{{ $.Values.deployment.image_exporter.repository }}:{{ $.Values.deployment.image_exporter.tag }}"
          imagePullPolicy: {{ $.Values.deployment.image_exporter.pullPolicy }}
          ports:
            - name: "beat-metrics"
              containerPort: 9479
          readinessProbe:
            httpGet:
              port: "beat-metrics"
              path: "/"
          resources:
            {{- toYaml $.Values.deployment.resources_exporter | nindent 12 }}
        - name: mtail
          image: "{{ $.Values.deployment.image_mtail.repository }}:{{ $.Values.deployment.image_mtail.tag }}"
          imagePullPolicy: {{ $.Values.deployment.image_mtail.pullPolicy }}
          command:
            - /usr/bin/mtail
            - -progs
            - /etc/configmap/progs
            - -logs
            - /volumes/emptydir/pubsubbeat.log*
            - --logtostderr
          ports:
            - name: "mtail-metrics"
              containerPort: 3903
          readinessProbe:
            httpGet:
              port: "mtail-metrics"
              path: "/"
          resources:
            {{- toYaml $.Values.deployment.resources_mtail | nindent 12 }}
          volumeMounts:
            - name: config-mtail
              mountPath: /etc/configmap/progs
            - name: emptydir
              mountPath: /volumes/emptydir/
      volumes:
        - name: config-files
          configMap:
            name: {{ include "pubsubbeat.fullname" $ }}-{{ $instance.topic }}
        - name: config-mtail
          configMap:
            name: {{ include "pubsubbeat.fullname" $ }}-mtail
        - name: emptydir
          emptyDir: {}
      affinity:
        {{- toYaml $.Values.affinity | nindent 8 }}
      nodeSelector:
        {{- toYaml $.Values.nodeSelector | nindent 8 }}
      {{- if $.Values.topologySpreadConstraints.enabled }}
      topologySpreadConstraints:
        {{- range $_, $constraint := $.Values.topologySpreadConstraints.constraints }}
        - maxSkew: {{ $constraint.maxSkew }}
          topologyKey: {{ $constraint.topologyKey }}
          whenUnsatisfiable: {{ $constraint.whenUnsatisfiable }}
          labelSelector:
            matchLabels:
              {{- include "pubsubbeat.selectorLabels" $ | nindent 14 }}
              topic: {{ $instance.topic }}
        {{- end }}
      {{- end }}
{{- end }}
