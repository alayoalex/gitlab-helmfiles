---
instances:
- topic: pubsub-consul-inf-ops
- topic: pubsub-fluentd-inf-ops
- topic: pubsub-gitaly-inf-ops
- topic: pubsub-gke-audit-inf-ops
- topic: pubsub-gke-inf-ops
- topic: pubsub-gke-systemd-inf-ops
- topic: pubsub-monitoring-inf-ops
- topic: pubsub-pages-inf-ops
- topic: pubsub-postgres-inf-ops
- topic: pubsub-pubsubbeat-inf-ops
- topic: pubsub-rails-inf-ops
- topic: pubsub-redis-inf-ops
- topic: pubsub-registry-inf-ops
- topic: pubsub-runner-inf-ops
- topic: pubsub-shell-inf-ops
- topic: pubsub-sidekiq-inf-ops
- topic: pubsub-system-inf-ops
- topic: pubsub-vault-inf-ops
- topic: pubsub-workhorse-inf-ops

deployment:
  resources:
    limits:
      cpu: 250m
      memory: 1G
    requests:
      cpu: 250m
      memory: 1G
  env:
    GOMAXPROCS: "1"
  resources_exporter:
    limits:
      cpu: 250m
      memory: 100M
    requests:
      cpu: 250m
      memory: 100M
  resources_mtail:
    limits:
      cpu: 250m
      memory: 100M
    requests:
      cpu: 250m
      memory: 100M

serviceAccount:
  annotations:
    iam.gke.io/gcp-service-account: "pubsubbeat-k8s@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com"

pubsubbeat:
  es:
    installed: {{ .Values | getOrNil "pubsubbeat.es.installed" | default false }}
  json:
    use_number: true

secrets:
  values:
{{ if .Values | getOrNil "local_secrets" | default false -}}
{{ readFile "private/secrets.yaml" | indent 4 -}}
{{- else -}}
{{ exec "bash" (list "-c" "gsutil cat gs://gitlab-configs/ops_pubsubbeat.yaml.enc | gcloud --project gitlab-ops kms decrypt --location global --keyring=gitlab-shared-configs --key config --ciphertext-file=- --plaintext-file=-") | indent 4 -}}
{{- end -}}
