---
{{ $env_prefix := .Environment.Values | getOrNil "env_prefix" | default .Environment.Name }}
{{ $monitoring_env := .Environment.Values | getOrNil "monitoring_env" | default $env_prefix }}
{{ $cluster := .Environment.Values | getOrNil "cluster" | default (printf "%s-gitlab-gke" .Environment.Name) }}
{{ $region := .Environment.Values | getOrNil "region" | default "us-east1" }}

# https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack#migrating-from-stableprometheus-operator-chart
nameOverride: prometheus-operator

defaultRules:
  create: false

coreDns:
  enabled: false
grafana:
  enabled: false
kubeDns:
  enabled: true
  serviceMonitor:
    relabelings:
      - targetLabel: type
        replacement: kubernetes
      - targetLabel: tier
        replacement: inf
      - targetLabel: stage
        replacement: main
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
kubeEtcd:
  enabled: false
kubeProxy:
  enabled: false
kubeScheduler:
  enabled: false

alertmanager:
  enabled: {{ .Values | getOrNil "gitlab_monitoring.alertmanager.installed" | default false }}
  alertmanagerSpec:

    # Managed in https://gitlab.com/gitlab-com/runbooks/-/tree/master/alertmanager
    configSecret: "alertmanager-config"
    useExistingSecret: true

    image:
      tag: v0.22.2

    logFormat: json
    resources:
      requests:
        cpu: 250m
        memory: 512Mi
      limits:
        memory: 512Mi
  service:
    type: "NodePort"
    annotations:
      cloud.google.com/backend-config: '{"default": "alertmanager-gcloud-ingress-backend"}'
  serviceMonitor:
    relabelings:
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.global-static-ip-name: "alertmanager-ingress-iap-{{ .Environment.Name }}"
      networking.gke.io/managed-certificates: "alertmanager"
      networking.gke.io/v1beta1.FrontendConfig: "alertmanager"
{{- if not (.Values | getOrNil "gitlab_monitoring.google_managed_cert.enabled" | default false) }}
    tls:
      - secretName: alertmanager-gke-tls
{{- end }}

prometheusOperator:
  logFormat: json
  serviceMonitor:
    relabelings:
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
  # No tag exists that supports the new alertmanager config, use specific SHA at the moment
  # Git commit: https://github.com/prometheus-operator/prometheus-operator/commit/0a38647379a5e93f639bf8e634deabcc32e01fb6
  # Image SHA: https://github.com/prometheus-operator/prometheus-operator/pkgs/container/prometheus-operator/8929147
  # Remove in: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14419
  image:
    repository: ghcr.io/prometheus-operator/prometheus-operator
    tag: "master"
    sha: "bb79240165868c7d73d3db2b45bd065bf2b3050729aa4809f6de79cace232feb"
    pullPolicy: IfNotPresent
  thanosImage:
    tag: v0.26.0

prometheus:
  prometheusSpec:
    replicas: 2
    walCompression: true
    containers:
      - name: prometheus
        readinessProbe:
          httpGet:
            path: /-/ready
            port: web
            scheme: HTTP
          failureThreshold: 120
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 3
        startupProbe:
          httpGet:
            path: /-/ready
            port: web
            scheme: HTTP
          failureThreshold: 60
          periodSeconds: 60
          successThreshold: 1
          timeoutSeconds: 3
    additionalAlertManagerConfigs:
      - api_version: v2
        dns_sd_configs:
          - names: ["alertmanager-internal.ops.gke.gitlab.net"]
            type: "A"
            port: 9093
            refresh_interval: "10s"
    externalLabels:
      provider: gcp
      env: {{ $monitoring_env }}
      environment: {{ $monitoring_env }}
      monitor: default
      region: {{ $region }}
      cluster: {{ $cluster }}
    image:
      tag: v2.34.0
    logFormat: json
    {{- if not (contains "gprd" .Environment.Name) }}
    nodeSelector:
      type: default
    {{- end }}
    retention: 1d  # 1 day
    scrapeInterval: 15s
    evaluationInterval: 15s
    thanos:
      version: v0.26.0
      objectStorageConfig:
        key: objstore.yaml
        name: thanos-objstore-config
    podAntiAffinity: "hard"
    affinity:
      nodeAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 1
            preference:
              matchExpressions:
              - key: type
                operator: In
                values:
                - default
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
            - matchExpressions:
              - key: beta.kubernetes.io/instance-type
                operator: NotIn
                values:
                  - c2-standard-4
                  - c2-standard-8
                  - c2-standard-16
                  - c2-standard-32
                  - c2-standard-64
    serviceMonitorSelectorNilUsesHelmValues: false
    serviceMonitorNamespaceSelector:
      matchExpressions:
        - key: name
          operator: In
          values: [delivery, jaeger, logging, monitoring, pagespeed, pubsubbeat, woodhouse, gcp-quota-exporter, redis, camoproxy, vault]
    podMonitorSelectorNilUsesHelmValues: false
    podMonitorNamespaceSelector:
      matchExpressions:
        - key: name
          operator: In
          values: [monitoring, jaeger, logging, pubsubbeat, redis, camoproxy, vault]
    ruleSelectorNilUsesHelmValues: false
    ruleNamespaceSelector:
      matchExpressions:
        - key: name
          operator: In
          values: [monitoring, jaeger, logging, pubsubbeat, redis, camoproxy]

  podDisruptionBudget:
    enabled: true
    minAvailable: 1

  service:
    additionalPorts:
      - name: thanos-sidecar-grpc
        port: 10901
        targetPort: 10901
      - name: thanos-sidecar-metrics
        port: 10902
        targetPort: 10902
    type: "LoadBalancer"
    annotations:
      cloud.google.com/load-balancer-type: "Internal"
      cloud.google.com/backend-config: '{"default": "prometheus-gcloud-ingress-backend"}'
  serviceMonitor:
    relabelings:
      - targetLabel: type
        replacement: monitoring
      - targetLabel: tier
        replacement: inf
      - targetLabel: stage
        replacement: main
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.global-static-ip-name: "prometheus-ingress-iap-{{ .Environment.Name }}"
      networking.gke.io/managed-certificates: "prometheus"
{{- if not (.Values | getOrNil "gitlab_monitoring.google_managed_cert.enabled" | default false) }}
    tls:
      - secretName: prometheus-gke-tls
{{- end }}

kubelet:
  serviceMonitor:
    cAdvisorRelabelings:
      - sourceLabels: [__metrics_path__]
        targetLabel: metrics_path
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
    probesRelabelings:
      - sourceLabels: [__metrics_path__]
        targetLabel: metrics_path
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node
    resource: false
    relabelings:
      - targetLabel: type
        replacement: kubernetes
      - targetLabel: tier
        replacement: inf
      - targetLabel: stage
        replacement: main
      - sourceLabels: [__metrics_path__]
        targetLabel: metrics_path
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node

kubeControllerManager:
  serviceMonitor:
    relabelings:
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node

kubeStateMetrics:
  serviceMonitor:
    relabelings:
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node

nodeExporter:
  serviceMonitor:
    relabelings:
      - sourceLabels:
          - __meta_kubernetes_pod_node_name
        targetLabel: node

prometheus-node-exporter:
  image:
    tag: v1.0.1
  extraArgs:
    - --collector.filesystem.ignored-mount-points=^/(dev|proc|sys|var/lib/docker/.+)($|/)
    - --collector.filesystem.ignored-fs-types=^(autofs|binfmt_misc|cgroup|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|mqueue|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|sysfs|tracefs)$
    - --log.format=json

kube-state-metrics:
  metricLabelsAllowlist:
    - ingresses=[*]
    - nodes=[*]
    - pods=[*]
    - horizontalpodautoscalers=[*]
    - deployments=[*]
