---
nameOverride: "pvs-stackdriver-exporter"

deployment:
  image:
    repository: "prometheuscommunity/stackdriver-exporter"
    tag: "v0.11.0"
  env:
    STACKDRIVER_EXPORTER_GOOGLE_PROJECT_ID: "{{ .Values.pvs.projectid }}"
    STACKDRIVER_EXPORTER_MONITORING_METRICS_TYPE_PREFIXES: "{{ .Values.pvs.metrics }}"
  ports:
    - name: "metrics"
      containerPort: 9255
  probes:
    readiness:
      httpGet:
        port: "metrics"
        path: "/metrics"
  resources:
    requests:
      cpu: 250m
      memory: 256Mi
    limits:
      memory: 1Gi

service:
  enabled: true
  headless: true
  ports:
    - name: "metrics"
      targetPort: "metrics"
      port: 9255

prometheus_monitoring:
  enabled: true
  scrape_interval: "30s"
  relabelings:
    - targetLabel: type
      replacement: sv
    - targetLabel: tier
      replacement: ci
    - targetLabel: stage
      replacement: main
    - sourceLabels:
        - __meta_kubernetes_pod_node_name
      targetLabel: node

serviceAccount:
  create: true
  annotations:
    iam.gke.io/gcp-service-account: "pvs-k8s@{{ .Environment.Values.google_project }}.iam.gserviceaccount.com"

secrets:
  values:
