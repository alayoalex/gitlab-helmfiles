# pvs-stackdriver-exporter release

## Purpose
This release scrapes Trust and Safety Pipeline Verification Service (PVS)
metrics from stackdriver. For reference, the
[issue](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/12964)
that spawned this work.

## Development
Due to the methods of providing access for the exporter to fetch metrics from
stackdriver, it is not easy to test or simulate that access in a minikube
environment. You should probably consider using GSTG as your testing
environment.

## Values
* `projectid` is the GCP Project ID for the stackdriver location to fetch
  metrics from.
* `metrics` are the list of metrics to fetch for export.

These and other possible flags are documented better
[here](https://github.com/prometheus-community/stackdriver_exporter#flags).
