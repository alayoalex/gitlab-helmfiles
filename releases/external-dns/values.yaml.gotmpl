---
provider: "google"

commonLabels:
  type: external-dns
  stage: main
  tier: sv
  shard: default

serviceAccount:
  annotations:
    # It's a shame to have to write this gnarly template expression,
    # but we initially used a service account name prefix that when combined
    # with the new, longer, zonal env names, is longer than GCP's maximum.
    # Migrating the regional clusters to use a shorter prefix without downtime
    # is more work than this is worth, for now.
    {{- $username := default (printf "kube-external-dns-%s" .Environment.Name) (.Values.external_dns | getOrNil "service_account_name") }}
    iam.gke.io/gcp-service-account: "{{ $username }}@{{ .Values.google_project }}.iam.gserviceaccount.com"

domainFilters:
  - "{{ .Environment.Name }}.gke.gitlab.net"
policy: "sync"
txtOwnerId: "k8s-external-dns-{{ .Environment.Name }}."

publishInternalServices: true

sources:
  - service
  - ingress

# Default 1m, also use events to trigger updates
interval: 30s
triggerLoopOnEvent: true

nodeSelector:
  type: default
  iam.gke.io/gke-metadata-server-enabled: "true"

metrics:
  enabled: true
  serviceMonitor:
    enabled: true
